# soniks-mcc

SONIKS Mission Control Center

СОНИКС Центр управления полетами (ЦУП)

## Описание проекта

СОНИКС ЦУП - раздел сайта sonik.space, представляющий из себя платформу для управления и контроля открытых, образовательных и радиолюбительских спутниковых миссий. 

## Правила использования

Ответственный радиолюбитель-оператор может свободно пользоваться платформой для управления собственными спутниками при подключении собственной радиолюбительской станции управления к сети. Управление может осуществляться через любую активную станцию управления, подключенную к сети. При этом собственная станция оператора должна иметь хотя бы один успешный сеанс связи со спутником и быть активной на момент необходимого ему сеансу связи. Ответственным радиолюбителем-оператором может считаться только тот человек, чье имя указано в заявке IARU (МСР) в поле `Responsible Operator`. Иные ситуации рассматриваются в частном порядке.

Для управления собственным спутником должен быть предоставлен протокол управления, хотя бы те команды, отправка которых плариуется на спутник, используя инструменты сети. Реализация протокола по инструкции осуществляется оператором спутника. Сетью СОНИКС гарантируется конфиденциальность хранения переданного протокола управления, при этом все риски использования удаленных станций пользователь берет на себя (сеть может обпеспечить безопасность соединения сервер-клиент, а также конфиденциальность передачи пакетов на приемник, однако в открытом радиолюбительском диапазоне могут быть риски, от которых никто не может защитить).

## Лицензия

[![CC BY-NC-ND 4.0][cc-by-nc-nd-shield]][cc-by-nc-nd]

Данная работа распространяется под лицензией
[Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International License][cc-by-nc-nd].

[![CC BY-NC-ND 4.0][cc-by-nc-nd-image]][cc-by-nc-nd]

[cc-by-nc-nd]: http://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-by-nc-nd-image]: https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png
[cc-by-nc-nd-shield]: https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg